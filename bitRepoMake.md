#bitRepoMake
##Usage Instructions
------------------------
This program allows you to easily create a bitbucket git repo from the command line.

Best use of this program in git bash requires making an alias (I use repomake).

-To make the alias, put *repomake.sh* in your home directory (~/)
-Then create a .bashrc file
>touch .bashrc
-In that .bashrc file create the following alias:
>alias repomake='~/repomake.sh'
-Save the file and exit the current git bash session
-When you reopen git bash, you will be told you are missing a .bash_profile file and the appropriate one will be made for you
-Now you can just call *repomake* from any location to create a repo with the name of your choosing
---Note: you will still have to go to your working directory to actually make commits and pulls---
